# TYPO3 Extension: CkEditor BE-Config

**What does it do**
It integrates CK-Editor YAML Configuration in TYPO3-Backend:<br />
```Configuration > CK-Editor YAML Configuration```


**Installation:**<br />
```composer req typoworx/tnm-ckeditor-beconfig```