<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'IF CkEditor BE-Config',
    'description' => 'Configuration Provider to debug CK-Editor YAML in Backend>Configuration',
    'category' => 'plugin',
    'author' => 'Alexander Büchner, Christian Händel, Gabriel Kaufmann',
    'author_email' => 'info@interfrog.de',
    'author_company' => 'Interfrog Produktion GmbH',
    'state' => 'beta',
    'version' => '0.5',
    'constraints' => [
        'depends' => [
            'typo3' => '10.5-12.5'
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'autoload' => [
        'psr-4' => [
            'TYPOworx\TnmCkeditorAddons\\' => 'Classes'
        ],
    ],
];
